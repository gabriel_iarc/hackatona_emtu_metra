class CreateOldUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :old_users do |t|
      t.string :card_number   , null: false, default: ""
      t.string :name          , null: false, default: ""
      t.string :cpf           , null: false, default: ""
      t.timestamps
    end
  end
end
