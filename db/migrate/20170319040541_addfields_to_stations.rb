class AddfieldsToStations < ActiveRecord::Migration[5.0]
  def change
    add_column :stations, :nome, :string, null: false, default: ""
    add_column :stations, :latitude, :string, null: false, default: ""
    add_column :stations, :longitude, :string, null: false, default: ""
    add_column :stations, :aberto, :boolean, null: false, default: false
  end
end
