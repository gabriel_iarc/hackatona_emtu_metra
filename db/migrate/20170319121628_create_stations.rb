class CreateStations < ActiveRecord::Migration[5.0]
  def change
    create_table :stations do |t|
      t.string :name
      t.string :latitude
      t.string :longitude
      t.boolean :active

      t.timestamps
    end
  end
end
