class CreateSlots < ActiveRecord::Migration[5.0]
  def change
    create_table :slots do |t|
      t.string :station_id
      t.string :name
      t.boolean :park

      t.timestamps
    end
  end
end
