json.extract! slot, :id, :station_id, :name, :park, :created_at, :updated_at
json.url slot_url(slot, format: :json)
