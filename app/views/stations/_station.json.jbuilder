json.extract! station, :id, :name, :latitude, :longitude, :active, :created_at, :updated_at
json.url station_url(station, format: :json)
