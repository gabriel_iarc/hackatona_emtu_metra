Rails.application.routes.draw do

  devise_for :admins
  resources :slots
  resources :stations
  resources :titulos
  root 'site#index'

  resources :site


  
  resources :old_users

  resources :station
  
  post '/station/new(:station)'       => "station#create"

  resources :slot

  post '/slot/new(:slot)'       => "slot#create"
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
